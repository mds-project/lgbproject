# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "subscribe: 1 messages, 0 services")

set(MSG_I_FLAGS "-Isubscribe:/home/nvidia/catkin_ws/src/subscribe/msg;-Istd_msgs:/opt/ros/melodic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(subscribe_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/nvidia/catkin_ws/src/subscribe/msg/subscribe_msg.msg" NAME_WE)
add_custom_target(_subscribe_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "subscribe" "/home/nvidia/catkin_ws/src/subscribe/msg/subscribe_msg.msg" ""
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(subscribe
  "/home/nvidia/catkin_ws/src/subscribe/msg/subscribe_msg.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/subscribe
)

### Generating Services

### Generating Module File
_generate_module_cpp(subscribe
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/subscribe
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(subscribe_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(subscribe_generate_messages subscribe_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/nvidia/catkin_ws/src/subscribe/msg/subscribe_msg.msg" NAME_WE)
add_dependencies(subscribe_generate_messages_cpp _subscribe_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(subscribe_gencpp)
add_dependencies(subscribe_gencpp subscribe_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS subscribe_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(subscribe
  "/home/nvidia/catkin_ws/src/subscribe/msg/subscribe_msg.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/subscribe
)

### Generating Services

### Generating Module File
_generate_module_eus(subscribe
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/subscribe
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(subscribe_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(subscribe_generate_messages subscribe_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/nvidia/catkin_ws/src/subscribe/msg/subscribe_msg.msg" NAME_WE)
add_dependencies(subscribe_generate_messages_eus _subscribe_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(subscribe_geneus)
add_dependencies(subscribe_geneus subscribe_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS subscribe_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(subscribe
  "/home/nvidia/catkin_ws/src/subscribe/msg/subscribe_msg.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/subscribe
)

### Generating Services

### Generating Module File
_generate_module_lisp(subscribe
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/subscribe
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(subscribe_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(subscribe_generate_messages subscribe_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/nvidia/catkin_ws/src/subscribe/msg/subscribe_msg.msg" NAME_WE)
add_dependencies(subscribe_generate_messages_lisp _subscribe_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(subscribe_genlisp)
add_dependencies(subscribe_genlisp subscribe_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS subscribe_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(subscribe
  "/home/nvidia/catkin_ws/src/subscribe/msg/subscribe_msg.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/subscribe
)

### Generating Services

### Generating Module File
_generate_module_nodejs(subscribe
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/subscribe
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(subscribe_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(subscribe_generate_messages subscribe_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/nvidia/catkin_ws/src/subscribe/msg/subscribe_msg.msg" NAME_WE)
add_dependencies(subscribe_generate_messages_nodejs _subscribe_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(subscribe_gennodejs)
add_dependencies(subscribe_gennodejs subscribe_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS subscribe_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(subscribe
  "/home/nvidia/catkin_ws/src/subscribe/msg/subscribe_msg.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/subscribe
)

### Generating Services

### Generating Module File
_generate_module_py(subscribe
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/subscribe
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(subscribe_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(subscribe_generate_messages subscribe_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/nvidia/catkin_ws/src/subscribe/msg/subscribe_msg.msg" NAME_WE)
add_dependencies(subscribe_generate_messages_py _subscribe_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(subscribe_genpy)
add_dependencies(subscribe_genpy subscribe_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS subscribe_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/subscribe)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/subscribe
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(subscribe_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/subscribe)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/subscribe
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(subscribe_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/subscribe)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/subscribe
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(subscribe_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/subscribe)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/subscribe
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(subscribe_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/subscribe)
  install(CODE "execute_process(COMMAND \"/usr/bin/python2\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/subscribe\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/subscribe
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(subscribe_generate_messages_py std_msgs_generate_messages_py)
endif()
