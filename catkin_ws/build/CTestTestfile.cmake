# CMake generated Testfile for 
# Source directory: /home/nvidia/catkin_ws/src
# Build directory: /home/nvidia/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("subscribe")
subdirs("darknet_ros/darknet_ros_msgs")
subdirs("darknet_ros/darknet_ros")
subdirs("usb_cam")
