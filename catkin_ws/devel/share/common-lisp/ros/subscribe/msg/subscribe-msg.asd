
(cl:in-package :asdf)

(defsystem "subscribe-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "subscribe_msg" :depends-on ("_package_subscribe_msg"))
    (:file "_package_subscribe_msg" :depends-on ("_package"))
  ))